package filter;

import entity.User;
import service.Impl.UserServiceImpl;
import service.UserService;
import utils.Base64Utils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/login.jsp")
public class AutoLoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletResponse response = (HttpServletResponse)resp;
        HttpServletRequest request = (HttpServletRequest)req;
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("userInfo")){
                    String value = Base64Utils.decode(cookie.getValue());
                    String[] split = value.split("#");
                    UserService userService = new UserServiceImpl();
                    User user = userService.loginUser(split[0], split[1]);
                    if(user != null){
                        request.getSession().setAttribute("loginUser",user);
                        response.sendRedirect(request.getContextPath() + "/index.jsp");
                    }else {
                        chain.doFilter(req, resp);
                    }
                }else {
                    chain.doFilter(req, resp);
                }
            }
        }else {
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
