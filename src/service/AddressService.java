package service;

import entity.Address;

import java.util.List;

public interface AddressService {
    List<Address> getAllAddressByUid(int uid);
    int insertAddress(Address address);
    int updateAddress(Address address);
    int deleteAddress(int id);
    int setDefaultAddress(int uid ,int id);
}
