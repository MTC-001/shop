package service;

import entity.Orders;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月25日8:52
 */
public interface OrdersService {
    int createOrders(Orders orders);
    List<Orders> selectAllOrders(int id);
    void changeStatus(String oid);
    Orders selectOrdersByOid(String oid);
}
