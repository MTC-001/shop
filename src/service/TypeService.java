package service;

import entity.Type;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日8:46
 */
public interface TypeService {
    List<Type> getAllTypes();
}
