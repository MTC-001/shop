package service;

import entity.Cart;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日14:23
 */
public interface CartService {
    int addCart(int id, int gid);
    List<Cart> getCartById(int id);
    int deleteCartByGid(int id,int gid);
    int changeCart(Cart cart);
    int clearCart(int id);
}
