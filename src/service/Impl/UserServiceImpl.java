package service.Impl;

import dao.Impl.UserDaoImpl;
import dao.UserDao;
import entity.User;
import service.UserService;
import utils.DruidUtils;

/**
 * @author mtc
 * @date 2021年10月23日9:48
 */
public class UserServiceImpl implements UserService {
    UserDao userDao = new UserDaoImpl();

    //驗證
    @Override
    public User checkUsername(String username) {
        User user = null;
        try {
            DruidUtils.begin();
            user = userDao.selectByUsername(username);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return user;
    }

    //註冊
    @Override
    public int register(User user) {
        int result = 0;
        try {
            DruidUtils.begin();
            result = userDao.insertUser(user);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }

    //激活
    @Override
    public int active(String email, String code) {
        int result = 0;
        try {
            DruidUtils.begin();
            result = userDao.updateFlag(email,code);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }

    //登錄
    @Override
    public User loginUser(String username, String password) {
        User user = null;
        try {
            DruidUtils.begin();
            User result = userDao.selectByUsername(username);
            if(result != null && result.getPassword().equals(password)){
                user = result;
            }
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return user;
    }
}
