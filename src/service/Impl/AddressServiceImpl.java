package service.Impl;

import dao.AddressDao;
import dao.Impl.AddressDaoImpl;
import entity.Address;
import service.AddressService;
import utils.DruidUtils;

import java.util.List;

public class AddressServiceImpl implements AddressService {
    private AddressDao addressDao = new AddressDaoImpl();

    @Override
    public List<Address> getAllAddressByUid(int uid) {
        List<Address> addressList = null;
        try {
            DruidUtils.begin();
            addressList = addressDao.selectAll(uid);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return addressList;
    }

    @Override
    public int insertAddress(Address address) {
        int result = 0;
        try {
            DruidUtils.begin();
            result = addressDao.insert(address);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
                DruidUtils.close();
            }
        return result;
    }

    @Override
    public int updateAddress(Address address) {
        int result = 0;
        try {
            DruidUtils.begin();
            result = addressDao.update(address);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }

    @Override
    public int deleteAddress(int id) {
        int result = 0;
        try {
            DruidUtils.begin();
            result = addressDao.delete(id);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }

    @Override
    public int setDefaultAddress(int uid, int id) {
        int result = 0;
        try {
            DruidUtils.begin();
            //1.修改指定id的地址的level为1.
            result = addressDao.updateDefault(id);
            if(result > 0){
                //2.将当前用户的非指定id的地址改为0
                addressDao.updateCommns(id,uid);
            }
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }
}
