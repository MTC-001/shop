package service.Impl;

import dao.CartDao;
import dao.GoodsDao;
import dao.Impl.CartDaoImpl;
import dao.Impl.GoodsDaoImpl;
import entity.Cart;
import entity.Goods;
import service.CartService;
import utils.DruidUtils;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日14:23
 */
public class CartServiceImpl implements CartService {
    CartDao cartDao = new CartDaoImpl();
    GoodsDao goodsDao = new GoodsDaoImpl();
    Cart cart = new Cart();
    Goods goods = new Goods();
    int result = 0;
    @Override
    public int addCart(int id, int gid) {
        //判斷是否存在，不存在添加，存在更新
        try {
            DruidUtils.begin();
            cart = cartDao.existsCart(id, gid);
            goods = goodsDao.selectById(gid);
            if(cart != null){
                cart.setNum(cart.getNum() + 1);
                cart.setMoney(cart.getNum() * goods.getPrice());
                cart.setGoods(goods);
                result = cartDao.updateCart(cart);

            }else{
                Cart newCart = new Cart(id,gid,1,goods.getPrice());
                newCart.setGoods(goods);
                result = cartDao.insertCart(newCart);
            }
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }

    @Override
    public List<Cart> getCartById(int id) {
        List<Cart> carts = null;
        try {
            DruidUtils.begin();
            carts = cartDao.selectCartById(id);
            for (Cart c :
                    carts) {
                Goods goods = goodsDao.selectById(c.getGid());
                c.setGoods(goods);
            }
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return carts;
    }

    @Override
    public int deleteCartByGid(int id,int gid) {
        int result = 0;
        try {
            DruidUtils.begin();
            result = cartDao.deleteCart(id,gid);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }

    @Override
    public int changeCart(Cart cart) {
        int result = 0;
        try {
            DruidUtils.begin();
            result = cartDao.updateCart(cart);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }

    @Override
    public int clearCart(int id) {
        int result = 0;
        try{
            DruidUtils.begin();
            result = cartDao.truncateCartById(id);
            DruidUtils.commit();
        } catch (Exception e){
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return result;
    }
}
