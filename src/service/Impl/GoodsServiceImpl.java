package service.Impl;

import dao.GoodsDao;
import dao.Impl.GoodsDaoImpl;
import entity.Goods;
import entity.Page;
import service.GoodsService;
import utils.DruidUtils;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日10:02
 */
public class GoodsServiceImpl implements GoodsService {
    private GoodsDao goodsDao = new GoodsDaoImpl();
    @Override
    public List<Goods> getAllGoodsById(Page page, int typeid) {
        List<Goods> goods = null;
        try {
            DruidUtils.begin();
            goods = goodsDao.selectAllByPage(page, typeid);
            page.setTotalCount((int)goodsDao.selectCount(typeid));
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return goods;
    }

    @Override
    public Goods getGoodsById(int id) {
        Goods goods = null;
        try {
            DruidUtils.begin();
            goods = goodsDao.selectById(id);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return goods;
    }
}
