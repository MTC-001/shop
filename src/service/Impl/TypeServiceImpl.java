package service.Impl;

import dao.Impl.TypeDaoImpl;
import dao.TypeDao;
import entity.Type;
import service.TypeService;
import utils.DruidUtils;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日8:47
 */
public class TypeServiceImpl implements TypeService {
    @Override
    public List<Type> getAllTypes() {
        List<Type> types = null;
        TypeDao typeDao = new TypeDaoImpl();
        try {
            DruidUtils.begin();
            types = typeDao.selectAll();
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        }finally {
            DruidUtils.close();
        }
        return types;
    }
}
