package service.Impl;

import dao.CartDao;
import dao.Impl.CartDaoImpl;
import dao.Impl.OrdersDaoImpl;
import dao.OrdersDao;
import entity.Cart;
import entity.Orders;
import entity.OrdersDetail;
import service.OrdersService;
import utils.DruidUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mtc
 * @date 2021年10月25日8:53
 */
public class OrdersServiceImpl implements OrdersService {
    private OrdersDao ordersDao = new OrdersDaoImpl();

    @Override
    public int createOrders(Orders orders) {
        try {
            DruidUtils.begin();
            //插入訂單數據
            ordersDao.insertOrders(orders);
            List<OrdersDetail> ordersDetails = new ArrayList<>();
            CartDao cartDao = new CartDaoImpl();
            List<Cart> carts = cartDao.selectCartById(orders.getUid());
            for (Cart cart :
                    carts) {
                OrdersDetail ordersDetail = new OrdersDetail(cart.getGid(),orders.getId(),cart.getNum(),cart.getMoney());
                ordersDetails.add(ordersDetail);
            }
            ordersDao.insertOrdersDetail(ordersDetails);
            cartDao.truncateCartById(orders.getUid());
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        } finally {
            DruidUtils.close();
        }
        return 0;
    }

    @Override
    public List<Orders> selectAllOrders(int id) {
        List<Orders> ordersList = null;
        try {
            DruidUtils.begin();
            ordersList = ordersDao.selectOrdersById(id);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        } finally {
            DruidUtils.close();
        }
        return ordersList;
    }
    @Override
    public void changeStatus(String oid){
        try {
            DruidUtils.begin();
            ordersDao.updateByOid(oid);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        } finally {
            DruidUtils.close();
        }
    }

    @Override
    public Orders selectOrdersByOid(String oid) {
        Orders orders = null;
        try {
            DruidUtils.begin();
            //根據oid查詢訂單和訂單地址信息
            orders = ordersDao.selectOrdersByOid(oid);
            //根據oid查詢訂單詳情和商品信息
            List<OrdersDetail> ordersDetails = ordersDao.selectOrdersDetailByOid(oid);
            orders.setList(ordersDetails);
            DruidUtils.commit();
        } catch (Exception e) {
            DruidUtils.rollback();
            e.printStackTrace();
        } finally {
            DruidUtils.close();
        }
        return orders;
    }
}
