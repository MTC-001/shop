package service;

import entity.Goods;
import entity.Page;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日10:01
 */
public interface GoodsService {
    List<Goods> getAllGoodsById(Page page,int typeid);
    Goods getGoodsById(int id);
}
