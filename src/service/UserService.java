package service;

import entity.User;

/**
 * @author mtc
 * @date 2021年10月23日9:48
 */
public interface UserService {
    User checkUsername(String username);
    int register(User user);
    int active(String email,String code);
    User loginUser(String username,String password);
}
