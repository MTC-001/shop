package entity;

import java.util.Date;
import java.util.List;

/**
 * @author mtc
 * @date 2021年10月23日8:56
 */
public class Orders {
    private String id;
    private int uid;
    private int money;
    private int status;
    private Date time;
    private int aid;
    private Address address;
    private List<OrdersDetail> List;

    public List<OrdersDetail> getList() {
        return List;
    }

    public void setList(List<OrdersDetail> ordersDetails) {
        this.List = ordersDetails;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Orders() {
    }

    public Orders(String id, int uid, int money, int status, Date time, int aid) {
        this.id = id;
        this.uid = uid;
        this.money = money;
        this.status = status;
        this.time = time;
        this.aid = aid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }
}
