package entity;

import java.util.Date;

/**
 * @author mtc
 * @date 2021年10月23日8:56
 */
public class Goods {
    private int id;
    private String name;
    private String picture;
    private Date pubdate;
    private int price;
    private int star;
    private String info;
    private int typeid;

    public int getTypeid() {
        return typeid;
    }

    public void setTypeid(int typeid) {
        this.typeid = typeid;
    }

    public Goods() {
    }

    public Goods(String name,String picture, Date pubdate, int price, int star, String info) {
        this.name = name;
        this.picture = picture;
        this.pubdate = pubdate;
        this.price = price;
        this.star = star;
        this.info = info;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getPubdate() {
        return pubdate;
    }

    public void setPubdate(Date pubdate) {
        this.pubdate = pubdate;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
