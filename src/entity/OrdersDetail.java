package entity;

/**
 * @author mtc
 * @date 2021年10月23日8:57
 */
public class OrdersDetail {
    private int id;
    private int gid;
    private String oid;
    private int num;
    private int money;
    private Goods goods;

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public OrdersDetail() {
    }

    public OrdersDetail(int gid, String oid, int num, int money) {
        this.gid = gid;
        this.oid = oid;
        this.num = num;
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
