package entity;

/**
 * @author mtc
 * @date 2021年10月24日9:10
 */
public class Page {
    //頁碼
    private int pageIndex;
    //頁大小
    private int pageSize;
    //總條數
    private int totalCount;
    //總頁數
    private int totalPage;
    //起始行 數據庫查詢的頁碼 - 1 * 頁大小
    private int startRows;

    public Page(int pageIndex) {
        this(pageIndex,8);
    }

    public Page(int pageIndex, int pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.startRows = (pageIndex - 1) * pageSize;
    }

    public int getStartRows() {
        return startRows;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
        this.totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : (totalCount / pageSize) + 1;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public int getPageIndex() {
        return pageIndex;
    }
}
