package entity;

/**
 * @author mtc
 * @date 2021年10月23日8:56
 */
public class Cart {
    private int id;
    private int gid;
    private int num;
    private int money;
    private Goods goods;

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Cart() {
    }

    public Cart(int id, int gid, int num, int money) {
        this.id = id;
        this.gid = gid;
        this.num = num;
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
