package utils;

/**
 * @author mtc
 * @date 2021年10月23日11:25
 */
public class Constants {
    private Constants(){}
    /*
    頁面相關
     */
    public static final String FORWARD = "forward:";
    public static final String REDIRECT = "redirect:";
    /*
    用戶相關
     */
    //用戶激活，未激活，註銷
    public static final int USER_NOT_ACTIVE = 0;
    public static final int USER_ACTIVE=1;
    public static final int USER_EXIT=2;
    //管理員，普通用戶
    public static final int USER_MANAGER=1;
    public static final int USER_CUSTOMER=0;
}
