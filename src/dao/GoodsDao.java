package dao;

import entity.Goods;
import entity.Page;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日9:58
 */
public interface GoodsDao {
    List<Goods> selectAllByPage(Page page,int typeid);
    long selectCount(int typeid);
    Goods selectById(int id);
}
