package dao;

import entity.Address;

import java.util.List;

public interface AddressDao {
    List<Address> selectAll(int uid);
    int insert(Address address);
    int update(Address address);
    int delete(int id);
    int updateDefault(int id);
    int updateCommns(int id,int uid);

//    update address set level = 1 where id =?
//
//    update address set level = 0 where id= ? and uid = ?
}
