package dao;

import entity.Type;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日8:48
 */
public interface TypeDao {
    List<Type> selectAll();
}
