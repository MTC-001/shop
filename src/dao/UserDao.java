package dao;

import entity.User;

/**
 * @author mtc
 * @date 2021年10月23日9:41
 */
public interface UserDao {
    User selectByUsername(String username);
    int insertUser(User user);
    int updateFlag(String email,String code);
}
