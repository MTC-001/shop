package dao.Impl;

import dao.TypeDao;
import entity.Type;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import utils.DruidUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日8:51
 */
public class TypeDaoImpl implements TypeDao {
    private QueryRunner queryRunner = new QueryRunner();
    @Override
    public List<Type> selectAll() {
        try {
            return queryRunner.query(DruidUtils.getConnection(),"select * from type;",new BeanListHandler<>(Type.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
