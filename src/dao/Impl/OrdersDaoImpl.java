package dao.Impl;

import com.sun.deploy.util.OrderedHashSet;
import dao.OrdersDao;
import entity.Address;
import entity.Goods;
import entity.Orders;
import entity.OrdersDetail;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import utils.DruidUtils;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author mtc
 * @date 2021年10月25日8:55
 */
public class OrdersDaoImpl implements OrdersDao {
    private QueryRunner queryRunner = new QueryRunner();

    @Override
    public int insertOrders(Orders orders) {
        Object[] params = {orders.getId(),orders.getUid(),orders.getMoney(),orders.getStatus(),orders.getTime(),orders.getAid()};
        try {
            return queryRunner.update(DruidUtils.getConnection(),"insert into orders(id,uid,money,status,time,aid) values(?,?,?,?,?,?);",params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void insertOrdersDetail(List<OrdersDetail> ordersDetails) {
        Object[][] params = new Object[ordersDetails.size()][];
        String sql = "insert into ordersdetail(gid,oid,num,money) values (?,?,?,?);";
        for (int i = 0; i < ordersDetails.size(); i++) {
            OrdersDetail ordersDetail = ordersDetails.get(i);
            params[i] = new Object[]{ordersDetail.getGid(),ordersDetail.getOid(),ordersDetail.getNum(),ordersDetail.getMoney()};
        }
        try {
            queryRunner.batch(DruidUtils.getConnection(),sql,params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Orders> selectOrdersById(int uid) {
        List<Orders> ordersList = null;
        try {
            List<Map<String, Object>> maps = queryRunner.query(DruidUtils.getConnection(),
                    "select o.id, o.uid, o.money, o.status, o.time, o.aid," +
                            "a.detail, a.name, a.phone, a.uid, a.level " +
                            "from orders o ,address a " +
                            "where a.id = o.aid and o.uid = ?;", new MapListHandler(),
                    uid);
            if(maps == null){
                return null;
            }
            ordersList = new LinkedList<>();
            for (Map map :
                    maps) {
                Orders orders = new Orders();
                Address address = new Address();
                BeanUtils.populate(orders,map);
                BeanUtils.populate(address,map);
                orders.setAddress(address);
                ordersList.add(orders);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return ordersList;
    }

    @Override
    public void updateByOid(String oid) {
        try {
            queryRunner.update(DruidUtils.getConnection(),"update orders set status = 4 where oid = ?;",oid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Orders selectOrdersByOid(String oid) {
        Orders orders = null;
        try {
            Map<String, Object> maps = queryRunner.query(DruidUtils.getConnection(),
                    "select o.id, o.uid, o.money, o.status, o.time, o.aid," +
                            "a.detail, a.name, a.phone, a.uid, a.level " +
                            "from orders o ,address a " +
                            "where a.id = o.aid and o.id = ?;", new MapHandler(),
                    oid);
            if(maps == null){
                return null;
            }
            orders = new Orders();
            Address address = new Address();
            BeanUtils.populate(orders,maps);
            BeanUtils.populate(address,maps);
            orders.setAddress(address);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public List<OrdersDetail> selectOrdersDetailByOid(String oid) {
        List<OrdersDetail> ordersDetails = null;
        try {
            List<Map<String, Object>> maps = queryRunner.query(DruidUtils.getConnection(),
                    "select od.id, od.gid, od.oid, od.num, od.money," +
                            "gs.name, gs.pubdate, gs.picture, gs.price, gs.star, gs.info, gs.typeid " +
                            "from ordersdetail od, goods gs " +
                            "where gs.id = od.gid and od.oid = ?;", new MapListHandler(), oid);
            if(maps == null){
                return null;
            }
            ordersDetails = new ArrayList<>();
            for (Map<String, Object> map : maps) {
                OrdersDetail ordersDetail = new OrdersDetail();
                Goods goods = new Goods();
                BeanUtils.populate(ordersDetail,map);
                BeanUtils.populate(goods,map);
                ordersDetail.setGoods(goods);
                ordersDetails.add(ordersDetail);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return ordersDetails;
    }
}
