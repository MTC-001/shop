package dao.Impl;

import dao.GoodsDao;
import entity.Goods;
import entity.Page;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import utils.DruidUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日10:00
 */
public class GoodsDaoImpl implements GoodsDao {
    private QueryRunner queryRunner = new QueryRunner();
    @Override
    public List<Goods> selectAllByPage(Page page, int typeid) {
        try {
            return queryRunner.query(DruidUtils.getConnection(),"select * from goods where typeid = ? limit ?,?;",new BeanListHandler<>(Goods.class),typeid,page.getStartRows(),page.getPageSize());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long selectCount(int typeid) {
        try {
            return queryRunner.query(DruidUtils.getConnection(),"select count(*) from goods where typeid = ?",new ScalarHandler<>(),typeid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Goods selectById(int id) {
        try {
            return queryRunner.query(DruidUtils.getConnection(),"select * from goods where id = ?",new BeanHandler<>(Goods.class),id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
