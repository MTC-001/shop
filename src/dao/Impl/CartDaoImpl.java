package dao.Impl;

import dao.CartDao;
import entity.Cart;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import utils.DruidUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日14:22
 */
public class CartDaoImpl implements CartDao {
    private QueryRunner queryRunner = new QueryRunner();
    @Override
    public Cart existsCart(int id, int gid) {
        try {
            return queryRunner.query(DruidUtils.getConnection(),"select * from cart where id = ? and gid = ?;",new BeanHandler<>(Cart.class),id,gid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int insertCart(Cart cart) {
        try {
            return queryRunner.update(DruidUtils.getConnection(),"insert into cart values(?,?,?,?);",cart.getId(),cart.getGid(),cart.getNum(),cart.getMoney());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int updateCart(Cart cart) {
        try {
            return queryRunner.update(DruidUtils.getConnection(),"update cart set num = ?, money = ? where id = ? and gid = ?;",cart.getNum(),cart.getMoney(),cart.getId(),cart.getGid());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Cart> selectCartById(int id) {
        try {
            return queryRunner.query(DruidUtils.getConnection(),"select * from cart where id = ?;",new BeanListHandler<>(Cart.class),id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int deleteCart(int id,int gid) {
        try {
            return queryRunner.update(DruidUtils.getConnection(),"delete from cart where id = ? and gid = ?;",id,gid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int truncateCartById(int id) {
        try {
            return queryRunner.update(DruidUtils.getConnection(),"delete from cart where id = ?;",id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
