package dao.Impl;

import dao.UserDao;
import entity.User;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import utils.DruidUtils;

import java.sql.SQLException;

/**
 * @author mtc
 * @date 2021年10月23日9:42
 */
public class UserDaoImpl implements UserDao {
    private QueryRunner queryRunner = new QueryRunner();
    @Override
    public User selectByUsername(String username) {
        try {
            return queryRunner.query(DruidUtils.getConnection(),
                    "select * from user where username = ?;",new BeanHandler<>(User.class),username);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int insertUser(User user) {
        Object[] params = {user.getUsername(),user.getPassword(),user.getEmail(),user.getGender(),user.getFlag(),user.getRole(),user.getCode()};
        try {
            return queryRunner.update(DruidUtils.getConnection(),
                    "insert into user(username,password,email,gender,flag,role,code) values(?,?,?,?,?,?,?);",params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int updateFlag(String email, String code) {
        try {
            return queryRunner.update(DruidUtils.getConnection(),
                    "update user set flag = 1 where email = ? and code = ?;",email,code);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
