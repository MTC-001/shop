package dao;

import entity.Cart;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日14:16
 */
public interface CartDao {
    Cart existsCart(int id,int gid);
    int insertCart(Cart cart);
    int updateCart(Cart cart);
    List<Cart> selectCartById(int id);
    int deleteCart(int id,int gid);
    int truncateCartById(int id);
}
