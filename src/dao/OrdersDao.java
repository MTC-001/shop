package dao;

import entity.Orders;
import entity.OrdersDetail;

import java.util.List;

/**
 * @author mtc
 * @date 2021年10月25日8:55
 */
public interface OrdersDao {
    int insertOrders(Orders orders);
    void insertOrdersDetail(List<OrdersDetail> ordersDetails);
    List<Orders> selectOrdersById(int uid);
    void updateByOid(String oid);
    Orders selectOrdersByOid(String oid);
    List<OrdersDetail> selectOrdersDetailByOid(String oid);
}
