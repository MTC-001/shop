package controller;

import entity.Address;
import entity.User;
import service.AddressService;
import service.Impl.AddressServiceImpl;
import utils.Constants;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@WebServlet("/address")
public class AddressController extends BaseController {
    private AddressService addressService = new AddressServiceImpl();

    //展示所有地址
    public String show(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        if (loginUser == null) {
            request.setAttribute("msg", "添加购物车请先登录");
            return Constants.FORWARD + "/login.jsp";
        }
        int id = loginUser.getId();
        List<Address> addList = addressService.getAllAddressByUid(id);
        request.setAttribute("addList",addList);
        return Constants.FORWARD+"/self_info.jsp";
    }

    //添加地址
    public String add(HttpServletRequest request, HttpServletResponse response){
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String detail = request.getParameter("detail");
        String uid = request.getParameter("uid");
        Address address = new Address(detail,name,phone,Integer.valueOf(uid),0);

        addressService.insertAddress(address);
        return Constants.FORWARD+"/address?method=show";
    }

    //修改地址
    public String update(HttpServletRequest request, HttpServletResponse response){
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String detail = request.getParameter("detail");
        String id = request.getParameter("id");
        String level = request.getParameter("level");//万一修改的是默认地址，则不能再使用默认的0

        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        if (loginUser == null) {
            request.setAttribute("msg", "修改地址请先登录");
            return Constants.FORWARD + "/login.jsp";
        }
        int uid = loginUser.getId();
        Address address = new Address(Integer.valueOf(id),detail,name,phone,uid,Integer.valueOf(level));
        addressService.updateAddress(address);
        return  Constants.FORWARD+"/address?method=show";
    }

    //删除地址
    public String delete(HttpServletRequest request, HttpServletResponse response){
        String id = request.getParameter("id");
        addressService.deleteAddress(Integer.valueOf(id));
        return Constants.FORWARD+"/address?method=show";
    }

    //设置默认地址
    public String setDefault(HttpServletRequest request, HttpServletResponse response){
        String id = request.getParameter("id");
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        if (loginUser == null) {
            request.setAttribute("msg", "修改地址请先登录");
            return Constants.FORWARD + "/login.jsp";
        }
        int uid = loginUser.getId();

        addressService.setDefaultAddress(uid,Integer.valueOf(id));
        return Constants.FORWARD+"/address?method=show";
    }

}
