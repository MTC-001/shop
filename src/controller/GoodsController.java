package controller;

import entity.Goods;
import entity.Page;
import service.GoodsService;
import service.Impl.GoodsServiceImpl;
import utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPBinding;
import java.io.IOException;
import java.util.List;

@WebServlet("/goods")
public class GoodsController extends BaseController {
    public String showAll(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
        //收參
        String typeid = request.getParameter("typeid");
        String pageIndex = request.getParameter("pageIndex");
        if(pageIndex == null){
            pageIndex = "1";
        }
        Page page = new Page(Integer.valueOf(pageIndex));
        //調用業務
        GoodsService goodsService = new GoodsServiceImpl();
        List<Goods> allGoodsById = goodsService.getAllGoodsById(page, Integer.valueOf(typeid));
        //存入作用域
        request.setAttribute("page",page);
        request.setAttribute("glist",allGoodsById);
        return Constants.FORWARD + "/goodsList.jsp";
    }

    public String show(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
        String id = request.getParameter("id");
        GoodsService goodsService = new GoodsServiceImpl();
        Goods goods = goodsService.getGoodsById(Integer.valueOf(id));
        request.setAttribute("goods",goods);
        return  Constants.FORWARD + "/goodsDetail.jsp";
    }
}
