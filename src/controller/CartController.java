package controller;

import entity.Cart;
import entity.User;
import service.CartService;
import service.Impl.CartServiceImpl;
import utils.Constants;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日14:14
 */
@WebServlet("/cart")
public class CartController extends BaseController{
    private CartService cartService = new CartServiceImpl();

    //添加購物車
    public String add(HttpServletRequest request, HttpServletResponse response){
        //判斷是否登錄
        User user = (User)request.getSession().getAttribute("loginUser");
        if(user == null){
            request.setAttribute("msg","請先登錄");
            return Constants.FORWARD + "/login.jsp";
        }
        //收參
        String gid = request.getParameter("gid");
        //調用業務邏輯
        cartService.addCart(user.getId(), Integer.valueOf(gid));
        //跳轉頁面
        return Constants.FORWARD + "/cartSuccess.jsp";
    }

    //查看購物車
    public String show(HttpServletRequest request,HttpServletResponse response){
        //判斷是否登錄
        User user = (User)request.getSession().getAttribute("loginUser");
        if(user == null){
            request.setAttribute("msg","請先登錄");
            return Constants.FORWARD + "/login.jsp";
        }
        //調用業務查詢訂單信息
        List<Cart> carts = cartService.getCartById(user.getId());
        request.setAttribute("carts",carts);
        return Constants.FORWARD + "/cart.jsp";
    }

    //刪除某個商品
    public String delete(HttpServletRequest request,HttpServletResponse response){
        User user = (User)request.getSession().getAttribute("loginUser");
        if(user == null){
            request.setAttribute("msg","請先登錄");
            return Constants.FORWARD + "/login.jsp";
        }
        String gid = request.getParameter("gid");
        cartService.deleteCartByGid(user.getId(),Integer.valueOf(gid));
        return Constants.REDIRECT + "/cart?method=show";
    }

    //加減某個商品
    public String update(HttpServletRequest request,HttpServletResponse response){
        User user = (User)request.getSession().getAttribute("loginUser");
        if(user == null){
            request.setAttribute("msg","請先登錄");
            return Constants.FORWARD + "/login.jsp";
        }
        String gid = request.getParameter("gid");
        String num = request.getParameter("num");
        String money = request.getParameter("money");
        Cart cart = new Cart(user.getId(),Integer.valueOf(gid),Integer.valueOf(num),Integer.valueOf(money));
        cartService.changeCart(cart);
        return Constants.REDIRECT + "/cart?method=show";
    }
    //刪除全部商品
    public String clear(HttpServletRequest request,HttpServletResponse response){
        String id = request.getParameter("id");
        cartService.clearCart(Integer.valueOf(id));
        return Constants.REDIRECT + "/cart?method=show";
    }
}
