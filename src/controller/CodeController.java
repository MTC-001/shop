package controller;

import cn.dsna.util.images.ValidateCode;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author mtc
 * @date 2021年10月23日18:06
 */
@WebServlet("/code")
public class CodeController extends BaseController {
    public static void createCode(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        ValidateCode validateCode = new ValidateCode(100,35,4,10);
        String code = validateCode.getCode();
        request.getSession().setAttribute("code",code);
        validateCode.write(response.getOutputStream());
    }
    public static String checkCode(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        String inputCode = request.getParameter("code");
        String code = (String)request.getSession().getAttribute("code");
        if(!inputCode.isEmpty() && inputCode.equalsIgnoreCase(code)){
            return "0";
        }else {
            return "1";
        }
    }
}
