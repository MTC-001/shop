package controller;

import entity.User;
import service.Impl.UserServiceImpl;
import service.UserService;
import utils.Base64Utils;
import utils.Constants;
import utils.EmailUtils;
import utils.RandomUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@WebServlet("/user")
public class UserController extends BaseController {
    //驗證
    public String check(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //收參
        String username = request.getParameter("username");
        //調用業務邏輯產看用戶是否存在
        UserService userService = new UserServiceImpl();
        User user = userService.checkUsername(username);
        //根據業務邏輯返回結果執行操作
        if (user == null) {
            return "0";
        } else {
            return "1";
        }
    }

    //註冊
    public String register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //收參
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String gender = request.getParameter("gender");
        UserService userService = new UserServiceImpl();
        //封裝實體類對象
        User user = new User(username, password, email, gender, Constants.USER_NOT_ACTIVE, Constants.USER_CUSTOMER, RandomUtils.createActive());
        //調用業務邏輯
        int result = userService.register(user);
        //根據返回結果跳轉頁面，registerSuccess
        if (result == 0) {
            request.setAttribute("registerMsg", "註冊失敗!!!");
            return Constants.FORWARD + "/register.jsp";
        } else {
            EmailUtils.sendEmail(user);
            return Constants.REDIRECT + "/registerSuccess.jsp";
        }
    }

    //激活
    public String active(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
        //收參
        String e = request.getParameter("e");
        String c = request.getParameter("c");
        //解密
        String email = Base64Utils.decode(e);
        String code = Base64Utils.decode(c);
        UserService userService = new UserServiceImpl();
        int result = userService.active(email, code);
        if(result > 0){
            request.setAttribute("msg","激活成功");
        }else{
            request.setAttribute("msg","激活失敗");
        }
        return Constants.FORWARD + "/message.jsp";
    }

    //登錄
    public String login(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String auto = request.getParameter("auto");
        UserService userService = new UserServiceImpl();
        User user = userService.loginUser(username, password);
        if(user != null && user.getFlag() == 1){
            request.getSession().setAttribute("loginUser",user);
            if(auto != null){
                Cookie cookie = new Cookie("userInfo",Base64Utils.encode(username + "#" + password));
                cookie.setPath("/");
                cookie.setMaxAge(60*60*24*14);
                response.addCookie(cookie);
            }
            return Constants.REDIRECT + "/index.jsp";
        }else {
            request.setAttribute("msg","用戶名或密碼錯誤或未激活");
            return  Constants.FORWARD + "/login.jsp";
        }
    }

    //註銷
    public String logout(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        request.getSession().removeAttribute("loginUser");
        Cookie cookie = new Cookie("userInfo","");
        cookie.setPath("/");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        request.setAttribute("msg","註銷成功!!!");
        return Constants.FORWARD + "/login.jsp";
    }
}
