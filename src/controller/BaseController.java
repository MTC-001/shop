package controller;

import utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@WebServlet(name = "BaseController")
public class BaseController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String method = request.getParameter("method");
        //反射
        //1.獲取類對象
        Class aClass = this.getClass();
        try {
            //2.通過反射獲取方法                方法名
            Method method0 = aClass.getMethod(method, HttpServletRequest.class, HttpServletResponse.class);
            //3.通過反射執行方法                               參數1     參數2
            String result = (String)method0.invoke(this, request, response);
            if(result != null && result.trim().length() != 0){
                if(result.startsWith(Constants.FORWARD)){
                    String path = result.substring(result.indexOf(":") + 1);
                    request.getRequestDispatcher(path).forward(request,response);
                }else if(result.startsWith(Constants.REDIRECT)){
                    String path = result.substring(result.indexOf(":") + 1);
                    response.sendRedirect(request.getContextPath() + path);
                }else{
                    response.getWriter().println(result);
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
