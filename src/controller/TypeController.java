package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Type;
import service.Impl.TypeServiceImpl;
import service.TypeService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebEndpoint;
import java.io.IOException;
import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日8:15
 */
@WebServlet("/type")
public class TypeController extends BaseController{
    public static String getAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //調用業務層查詢所有
        TypeService typeService = new TypeServiceImpl();
        List<Type> allTypes = typeService.getAllTypes();
        //將返回的集合變爲json字符串嚮應給客戶端
        ObjectMapper objectMapper = new ObjectMapper();
        String s = objectMapper.writeValueAsString(allTypes);
        return s;
    }
}
