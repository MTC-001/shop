package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import entity.*;
import service.AddressService;
import service.CartService;
import service.Impl.AddressServiceImpl;
import service.Impl.CartServiceImpl;
import service.Impl.OrdersServiceImpl;
import service.OrdersService;
import utils.Constants;
import utils.RandomUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author mtc
 * @date 2021年10月24日19:01
 */
@WebServlet("/order")
public class OrderController extends BaseController{
    private OrdersService ordersService = new OrdersServiceImpl();

    //添加地址
    public String add(HttpServletRequest request, HttpServletResponse response){
        String uid = request.getParameter("uid");
        AddressService addressService = new AddressServiceImpl();
        List<Address> addList = addressService.getAllAddressByUid(Integer.valueOf(uid));
        CartService cartService = new CartServiceImpl();
        List<Cart> cartList = cartService.getCartById(Integer.valueOf(uid));
        request.setAttribute("cartList",cartList);
        request.setAttribute("addList",addList);
        return Constants.FORWARD + "order.jsp";
    }
    //創建訂單
    public String create(HttpServletRequest request,HttpServletResponse response){
        String uid = request.getParameter("uid");
        String sum = request.getParameter("sum");
        String aid = request.getParameter("aid");
        Orders orders = new Orders(RandomUtils.createOrderId(),
                Integer.valueOf(uid),
                Integer.valueOf(sum),
                1,
                new Date(),
                Integer.valueOf(aid));
        //調用業務邏輯
        ordersService.createOrders(orders);
        return Constants.REDIRECT + "/order?method=show";
    }
    //展示訂單
    public String show(HttpServletRequest request,HttpServletResponse response){
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        if (loginUser == null) {
            request.setAttribute("msg", "查詢訂單请先登录");
            return Constants.FORWARD + "/login.jsp";
        }
        int id = loginUser.getId();
        List<Orders> ordersList = ordersService.selectAllOrders(id);
        request.setAttribute("orderList",ordersList);
        return Constants.FORWARD + "/orderList.jsp";
    }
    //確認收穫
    public String changeStatus(HttpServletRequest request,HttpServletResponse response){
        String oid = request.getParameter("oid");
        ordersService.changeStatus(oid);
        return Constants.REDIRECT + "/order?method=show";
    }
    //訂單詳情
    public String showDetail(HttpServletRequest request,HttpServletResponse response){
        //收參
        String oid = request.getParameter("oid");
        Orders od = ordersService.selectOrdersByOid(oid);
        request.setAttribute("od",od);
        return Constants.FORWARD + "/orderDetail.jsp";
    }
    public String success(HttpServletRequest request,HttpServletResponse response){
        String oid = request.getParameter("oid");
        //獲取支付結果的json
        String result = request.getParameter("result");
        //解析json，封裝成WeiXin
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            WeiXin weiXin = objectMapper.readValue(result, WeiXin.class);
            String result_code = weiXin.getResult().getResult_code();
            if (result_code != null && result_code.equals("SUCCESS")){
                //修改訂單狀態
                //跳轉查詢所有訂單
            }
            //支付失敗
            //跳轉message提示信息

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
